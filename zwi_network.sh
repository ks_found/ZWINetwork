#!/bin/bash
# Create a mirror of ZWI files and push ZWI to the network. 
#
# Version 1.3, Jan 29, 2023
#       Added zwi_ini 
# Version 1.2, Jan 9, 2023
#	Move to local/share/zwi
# Version 1.1, Jun 22, 2022
# 	Support publisher selection
#
# S.Chekanov  + KSF

if [ ! -x /usr/bin/curl ] ; then
    command -v curl >/dev/null 2>&1 || { echo >&2 "Please install 'curl' or set it in your path. Aborting."; exit 1; }
fi

if [ ! -x /usr/bin/wget ] ; then
    command -v wget >/dev/null 2>&1 || { echo >&2 "Please install 'wget' or set it in your path. Aborting."; exit 1; }
fi


if [ ! -x /usr/bin/rsync ] ; then
    command -v rsync >/dev/null 2>&1 || { echo >&2 "Please install 'rsync' or set it in your path. Aborting."; exit 1; }
fi


if [[ "$(python3 -V)" =~ "Python 3" ]]; then
   echo "Python 3 is installed"
else
   echo "Python 3 is NOT installed. Exit"
   exit 1
fi

CUR_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export ZWI_DIR="$HOME/.local/share/zwi/"

if [ ! -x "$ZWI_DIR" ] ; then
    mkdir -p $ZWI_DIR
fi
cp -f $CUR_DIR/zwi_share.py $ZWI_DIR/  2>/dev/null 
cp -f $CUR_DIR/make_index.py $ZWI_DIR/ 2>/dev/null
cp -f $CUR_DIR/zwi_ini       $ZWI_DIR/ 2>/dev/null

chmod 755 $ZWI_DIR/*

cat >$ZWI_DIR/zwi_get <<EOL
#!/bin/bash
# Obtain ZWI files from the network

if [ \$# -eq 0 ]
then
     echo "No arguments given. Production mode for ZWI network" 
     DEFAULT_IN="https://encycloreader.org/db/ZWI/"
     DEFAULT_OUT="ZWI"
     python3 \$ZWI_DIR/zwi_share.py -q -m -i \$DEFAULT_IN -o \$DEFAULT_OUT
else
     python3 \$ZWI_DIR/zwi_share.py "\$@"
fi
# echo "All done!"
EOL
chmod 755 $ZWI_DIR/zwi_get


cat >$ZWI_DIR/zwi_demo <<EOL
#!/bin/bash
# Demo example to test ZWI files from 4 demo mirrors 
if [ \$# -eq 0 ]
then
     echo "No arguments given. This is a demo network used for illustration"
     DEFAULT_IN="http://69.197.158.246/site1/ZWI/"
     DEFAULT_OUT="ZWI"
     python3 \$ZWI_DIR/zwi_share.py -q -m -i \$DEFAULT_IN -o \$DEFAULT_OUT
else
     python3 \$ZWI_DIR/zwi_share.py "\$@"
fi
# echo "All done!"
EOL
chmod 755 $ZWI_DIR/zwi_demo



cat >$ZWI_DIR/zwi_index <<EOL
#!/bin/bash
# Index ZWI files located in the file system 
python3 \$ZWI_DIR/make_index.py "\$@"
# echo "All done!"
EOL
chmod 755 $ZWI_DIR/zwi_index



cat >$ZWI_DIR/zwi_push <<EOL
#!/bin/bash
# Push ZWI file to the ZWI Network

PASS=0123456789

if [ \$# -eq 0 ]
  then
    echo "No arguments. Please specify the ZWI file name and the password"
    exit;
fi

if [ \$# -eq 1 ]
  then
    ZFILE=\$1
    echo "ZWI file name: \$ZFILE"
    echo "No password is given. Assume sandbox  publisher"
fi

if [ \$# -eq 2 ]
  then
    ZFILE=\$1
    PASS=\$2
    echo "ZWI file name: \$ZFILE"
    echo "Password:  \$PASS"
fi

curl -X POST -F zwi=@\${ZFILE} -F pass=\${PASS}  https://encycloreader.org/upload/put.php
EOL
chmod 755 $ZWI_DIR/zwi_push

export PATH="$PATH:$ZWI_DIR" 
export PYTHONPATH="$ZWI_DIR:$PYTHONPATH"

echo "ZWI network commands are ready:"
echo "-------------------------------"
echo " 'zwi_demo'         - demo showing monitoring ZWI files from 5 demo repositories"
echo " 'zwi_get'          - download and monitor ZWI files from Encyclosphere"
echo " 'zwi_get -h'       - shows a list of options for download and mirroring files"
echo " 'zwi_index -h'     - index local directory with ZWI files"
echo " 'zwi_ini'          - initialize ZWI network export using a collection of ZWI files"
echo " 'zwi_push art.zwi' - push the file art.zwi to the sandbox of the network"
echo " 'zwi_push art.zwi [password]' - push the file art.zwi to the network"
