#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Create 2-level index files for the ZWI database. All times are in the UTC.  
# To make all index files for all publishers by rescanning ZWI file use: 
#        python3 make_index.py -i <path> where <path> points to the directory with ZWI files.
# To update the index of specific publishers and the global index file use:
#        python3 make_index.py -i <path> -p "publisher1,publisher2" 
# Use the comma-seprated list of publishers. Note scanning of directories for other publishers will not be performed
#
# This directory should ends as ZWI/en (without / at the end).
# This module can also update the index of a specific publisher  
# 
# S.V.Chekanov, H.Sanger (KSF)
#
# Version 1.2, Jan 4, 2032
# Version 1.1, Nov 13, 2022
# Version 1.0, Nov 10, 2022
#
import re,sys,io,os
import glob,csv,gzip
import requests
from time import time
import time
from operator import itemgetter
from pathlib import Path
import argparse
from os.path import exists
from datetime import datetime
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# create index file for the ZWI directory
# Optionally, specify list with excluded or allowed encyclopedias 
def createIndexFiles(basedirZWI, excluded=[], publishers=[], verbose=True):
  """ Create new index files from the directory basedirZWI, but excluding excluded[] 
      basedirZWI: str
             base directry with ZWI
      excluded: list
             list of exluded publishes
      publishers: list
             list of allowed publishes
      return: dict{}
              dictinary with the index 
  """

  if (basedirZWI.endswith("/en")==False  and basedirZWI.endswith("/en/")==False):
                              basedirZWI=basedirZWI+"/en"


  start = time.time()
  outfile=basedirZWI+"/index.csv.gz"
  data=[]

  nn=0
  if (verbose): print("Initial scan of directories")
  for f in glob.glob(basedirZWI+"/*/*", recursive=False):

    if (len(excluded)>0): 
      remove=False
      for ex in excluded:
         if (f.find(ex+"/")>-1): remove=True
      if (remove): continue

    if (len(publishers)>0):
      remove=True
      for ex in publishers:
         if (f.find(ex+"/")>-1): 
                     remove=False
      if (remove): continue

    if os.path.isdir(f):
          name=f.replace(basedirZWI,"") 
          name=name.strip()
          if (name.startswith("/")): name=name[1:len(name)]
          if (name.endswith("/")):   name=name[0:len(name)-1]
          if (len(name)<2): continue 
          xtime=os.path.getmtime(f) # modification time 
          data.append( [int(xtime),name,f] )
          nn=nn+1

  # sort according to time
  data.sort(key = lambda x: x[0],reverse=True)

  mainIndex={}
  dirnumber={}
  dirsizes={}
  if (verbose): print("Making local index files for",nn," encyclopedias")
  nn=0
  xsum=0
  for d in data:
    m_name=d[1].strip()  # short name of the directory, i.e. wikitia/wikitia.com 

    if (len(excluded)>0):
      for ex in excluded:
         remove=False
         if (m_name.find(ex+"/")>-1): remove=True
         if (remove): continue
    if (len(publishers)>0):
      for ex in publishers:
         remove=True
         if (m_name.find(ex+"/")>-1): remove=False
         if (remove): continue
    xdir=d[2]             # full path, i.e ZWI/en/wikitia/wikitia.com 
    localindex= xdir+".csv.gz"
    #print("Creating:"+localindex)
    output = gzip.open(localindex, 'w')
    sdata="";
    #slow
    #flist=glob.glob(xdir+"/*.zwi", recursive=False)
    #sorted_by_mtime_descending = sorted(flist, key=lambda t: -os.stat(t).st_mtime)

    flist=os.scandir(xdir+"/")
    articles=[]
    dir_bytes=0
    dir_number=0
    for item in  flist: 
       shortname=item.name
       if (shortname.endswith(".zwi")):
         shortname=shortname[0:len(shortname)-4]
         #path=item.path
         xsize=int(item.stat().st_size) 
         xtime=int(item.stat().st_mtime) # time of last modification. 

         #xtime=int(os.path.getmtime(f))
         #xsize=int(os.path.getsize(f))
         #shortname=Path(f).stem
         #fulllist.append(str(xtime)+"|"+str(xsize)+"|"+shortname)
         articles.append([xtime,xsize,shortname])
         nn=nn+1
         xsum=xsum+xsize
         dir_bytes=dir_bytes+xsize
         dir_number=dir_number+1

    #print("Create dictionary=",m_name)
    dirnumber[m_name]=dir_number
    dirsizes[m_name]=int(dir_bytes) # evaluate correct size of the directory
    articles.sort(key = lambda x: x[0],reverse=True)

    if (verbose): print("Creating:"+localindex,"with",len(articles),"articles")
    for entry in articles:
         line=str(entry[0])+"|"+str(entry[1])+"|"+entry[2]+"\n"
         output.write(line.encode('utf-8')) 
    output.close()
    os.system("chmod 777 "+localindex)
    mainIndex[m_name]=articles
    #print(m_name,len(articles) )

  if (verbose):
      print("")
      print("Total files=",nn," Total size=",xsum/1073741824.0," GB")

  globalIndex={}
  nn=0
  totsize=0
  if (verbose): print("Write new global index:",outfile)
  output = gzip.open(outfile, 'wb')
  xlines=""
  for d in data:
    xtime=str(d[0])
    xname=str(d[1])
    xnumber=dirnumber[xname]  # number of files 
    xsize=dirsizes[xname]     # size of this directory
    # clean-up
    xname=xname.replace(basedirZWI,"")
    if (xname.startswith("/")): xname=xname[1:len(xname)]
    if (xname.endswith("/")):   xname=xname[0:len(xname)-1]
    totsize=totsize+xsize 
    line=str(xtime)+"|"+str(xsize)+"|"+str(xnumber)+"|"+xname+"\n"
    articles=mainIndex[xname]
    globalIndex[xname] = [xtime,xsize,xnumber,articles]
    output.write(line.encode('utf-8'))
    nn=nn+1
  output.close()
  os.system("chmod 777 "+outfile)
  if (verbose): print("Total encyclopedias=",nn," Total size=",totsize/1073741824.0," GB")
  end = time.time()
  if (verbose): print("Used time=",end - start)

  return globalIndex


##############################################
### short summary of index
##############################################
def shortSummaryOfIndex(globalIndex):
    """ returns a short summary of the global index file 
        [publishers, size, n_articles]
    """
    ntot=0
    nsize=0
    npub=0
    for key in globalIndex:
            data=globalIndex[key]
            ntot=ntot+int(data[2]) 
            nsize=nsize+int(data[1])
            npub=npub+1
    return [npub,nsize,ntot]



###################################################
##################  writeIndexFiles ###############
def writeIndexFiles(basedirZWI,globalIndex,publist=[],verbose=True):
  """ Write new index files to the disk for the list of  publishers (or all by default)"""
  outfile=basedirZWI+"/index.csv.gz"

  nn=0
  totsize=0
  if (verbose): print("Update global index:",outfile)
  output = gzip.open(outfile, 'wb')
  xlines=""
  for key in globalIndex:
    d=globalIndex[key]
    xtime=str(d[0])
    xsize=str(d[1])
    xnumber=str(d[2])
    totsize=totsize+int(xsize) 
    line=str(xtime)+"|"+str(xsize)+"|"+str(xnumber)+"|"+key+"\n"
    output.write(line.encode('utf-8'))
    nn=nn+1
  output.close()
  os.system("chmod 777 "+outfile)
  if (verbose): 
      print("Total encyclopedias=",nn," Total size=",totsize/1073741824.0," GB")
      print("Update publisher's index files")
  for key in globalIndex:
    keys=key.split("/")
    consider=False
    if (keys[0] in publist):
                    consider=True;
    if (consider==False): continue 
      
    localindex=basedirZWI+"/"+key+".csv.gz"
    if (verbose): print("  Creating:"+localindex)
    output = gzip.open(localindex, 'w')
    entries=globalIndex[key]
    filllist=entries[3]
    if (verbose): print("  Write=",len(filllist),"entries from publisher",key)
    for en in filllist:
        txt=str(en[0])+"|"+str(en[1])+"|"+en[2]+"\n"
        output.write(txt.encode('utf-8'))
    output.close()
    os.system("chmod 777 "+localindex)

######################################################
# Update index files for the publisher
#####################################################
def updateIndexFiles(basedirZWI, publist=[],verbose=True):
   """ Update index files for the list of specific publisher. No scanning of other publishers """
   globalIndex=getIndex(basedirZWI) # get global index first
   #print("Update index:",basedirZWI,"for publishers:",publist)

   pubkeys=[]
   pubdata=[] 
   # fetch the publisher
   for key in globalIndex: 
        keys=key.split("/")
        if (keys[0] in publist):
              pubdata.append(globalIndex[key]) 
              pubkeys.append(key) # name of the publisher/directory 

   if (len(pubdata)==0): 
       print("The publishers:",publist,"are not found")
       return False 

   if (verbose): print("Following publishers to be updated:", pubkeys)
   dirnumber={}
   dirsizes={}
   xsum=0
   # now scan all the  files for this list of publishers
   for xdir in pubkeys:
      flist=os.scandir(basedirZWI+"/"+xdir+"/")
      dir_bytes=0
      dir_number=0
      fulllist=[]
      for item in  flist:
        shortname=item.name
        if (shortname.endswith(".zwi")):
          shortname=shortname[0:len(shortname)-4]
          xsize=int(item.stat().st_size)
          xtime=int(item.stat().st_mtime) # time of last modification.
          fulllist.append([xtime,xsize,shortname])
          xsum=xsum+xsize
          dir_bytes=dir_bytes+xsize
          dir_number=dir_number+1

      # sort according to date
      fulllist.sort(key = lambda x: x[0],reverse=True)

      # most recent time:
      recent=fulllist[0][0]
      globalIndex[xdir]=[recent,dir_bytes,dir_number, fulllist]
      if (verbose): print("  - Files updated:",len(fulllist),"Publisher:",xdir,"Updated on:",datetime.fromtimestamp(recent))

   # write new index files
   writeIndexFiles(basedirZWI,globalIndex,publist)
   return


# Read global index files for all publishers from local directory. 
# The structure is: dic{key}=list
def getIndex(basedirZWI, excluded=[], publishers=[], verbose=True):
      """ Return global index of all publishers using local directory 
          @input:
             basedirZWI - base directory 
             excluded - list of excluded publishers
             publishers - list of included publishers
      """

      outfile=basedirZWI+"/index.csv.gz"
      if (exists(outfile) == False): 
                             print("File not found",outfile)
                             sys.exit()
      if (verbose): print("Read:",outfile)
      # read main index file
      mainIndex={}
      with gzip.open(outfile,'rt') as f:
        for line in f:
           line=line.replace("\n","")
           lines=line.split("|")
           if (len(lines) !=4):  
               print("Problems in reading index file:",outfile)
               return None 
           if (len(excluded)>0):
             remove=False
             for ex in excluded:
                 if (lines[3].find(ex)>-1): remove=True
             if (remove): continue
           if (len(publishers)>0):
             remove=True
             for ex in publishers:
                 if (lines[3].find(ex)>-1): remove=False
             if (remove): continue

           mainIndex[lines[3]]=[ lines[0],lines[1], lines[2] ] 
      ntot=0
      globalIndex={}
      # read data for specific publishers
      for key in mainIndex:
          pub_index=basedirZWI+key+".csv.gz" 
          if (exists(pub_index) == False):
            print("File not found",pub_index," index is not valid!")
            return 
          articles=[]
          with gzip.open(pub_index,'rt') as f:
            for line in f:
              line=line.replace("\n","")
              lines=line.split("|")
              if (len(lines) !=3):
                  print("Problems reading index file:",pub_index)
                  return None 
              articles.append(lines)
              # print(lines)
              ntot=ntot+1
          #print(key,"gets",len(articles)," articles")
          xmap=mainIndex[key]
          globalIndex[key] = [xmap[0],xmap[1],xmap[2],articles]

      if (verbose): print("The size of local main index is {} articles".format(ntot),"in",len(globalIndex),"publishers")
      return globalIndex


# Read global index files for all publishers from a remote URL of the phorm ZWI/en.
# The structure is: dic{key}=list
def getIndexRemote(baseURL,excluded=[], publishers=[], verbose=True):
      """ Return global index of all publishers from remote directory 
          @input:  baseURL input URL that should end with ZWI/en
                  excluded - list of excluded publishers
                  publishers - list of included publishers
      """
      if (baseURL.endswith("en")==False  and baseURL.endswith("en/")==False):
                              baseURL=baseURL+"/en/"
      outfile=baseURL+"/index.csv.gz"
      # get main index first
      sessionTCP  =  requests.Session()
      if (verbose): print("Read:",outfile)
      web_response = sessionTCP.get(outfile,timeout=30, stream=True) 
      csv_gz_file = web_response.content # Content in bytes from requests.get
      f = io.BytesIO(csv_gz_file)
      mainIndex={} # main index
      with gzip.GzipFile(fileobj=f) as fh:
           reader = csv.reader(  io.TextIOWrapper(fh, 'utf8'),delimiter ="|",quoting=csv.QUOTE_MINIMAL)
           for lines in reader:
              if (len(lines) !=4):
                  print("Problems in reading index file:",outfile)
                  return None

              if (len(excluded)>0):
                remove=False
                for ex in excluded:
                    if (lines[3].find(ex)>-1): remove=True
                if (remove): continue

              if (len(publishers)>0):
                 remove=True
                 for ex in publishers:
                     if (lines[3].find(ex)>-1): remove=False
                 if (remove): continue
              # fill global index
              mainIndex[lines[3]]=[ lines[0],lines[1], lines[2] ]

      ntot=0
      globalIndex={}
      # read data for specific publishers
      for key in mainIndex:
          pub_index=baseURL+"/"+key+".csv.gz"
          web_response = sessionTCP.get(pub_index,timeout=30, stream=True)
          csv_gz_file = web_response.content # Content in bytes from requests.get
          f = io.BytesIO(csv_gz_file)
          articles=[]
          with gzip.GzipFile(fileobj=f) as fh:
            if (verbose): print("Load=",pub_index)
            reader = csv.reader(  io.TextIOWrapper(fh, 'utf8'),delimiter ="|",quoting=csv.QUOTE_MINIMAL)
            for lines in reader:
              if (len(lines) !=3):
                  print("Problems reading index file:",pub_index)
                  return None 
              articles.append(lines)
              # print(lines)
              ntot=ntot+1
          #print(key,"gets",len(articles)," articles")
          xmap=mainIndex[key]
          globalIndex[key] = [xmap[0],xmap[1],xmap[2],articles]

      sessionTCP.close()
      if (verbose): print("The size of main index from ",baseURL," is {} articles".format(ntot),"in",len(globalIndex),"publishers")
      return globalIndex


def printGlobalIndex(gindex, mess=""):
    """ print index in nice form"""
    gindex = {k: gindex[k] for k in sorted(gindex)}
    for key in  gindex:
            data=gindex[key]
            print()
            print(mess,"## publisher=",key," time=",data[0]," size=",data[1]," entries=",data[2])
            articles=data[3]
            res = sorted(articles, key=lambda tup: tup[2]) # list according to title 
            nn=1
            for a in res:
                print(nn,")",a[0],a[1],a[2])
                nn=nn+1


##################################################################
#########  Calculate Delta, ie. difference betwen two global indexes
#########  Return download list
###################################################################
def getDeltaDifference(newIndex, oldIndex, excluded=[], publishers=[], verbose=True):
  """ Calculate delta between two global indexes. It returns a dictionary where publisher is key, but 
      list are missing titles.
      @input: newIndex - new index (usually from remote site)
              oldIndex - old index (usually local)
              excluded - list of excluded publishers
              publishers - list of included publishers
      @return  list of articles with difference beween newIndex-oldIndex
  """

  # sorting according to keys
  oldIndex = {k: oldIndex[k] for k in sorted(oldIndex)}
  newIndex = {k: newIndex[k] for k in sorted(newIndex)}

  #dd1=newIndex["citizendium/citizendium.org"][3] 
  #dd2=oldIndex["citizendium/citizendium.org"][3] 

  #print(len(dd1),len(dd2))
  #print(len(newIndex["citizendium/citizendium.org"])) 
  #print(len(oldIndex["citizendium/citizendium.org"]))

  #print("New remote index=")
  #printGlobalIndex(newIndex,"New")
  #print("Old local index=")
  #printGlobalIndex(oldIndex,"Old")

  missing={}
  for key in newIndex:
         # print("Key in new index=",key)
         # skip encyclopedias that have identical entries
         if (len(key.strip())<1): continue

         if (len(excluded)>0):
             remove=False
             for ex in excluded:
                 if (key.find(ex)>-1): remove=True
             if (remove): continue

         if (len(publishers)>0):
             remove=True
             for ex in publishers:
                 if (key.find(ex)>-1): remove=False 
             if (remove): continue

         # process publishers that are common for new and old publishers
         if key in oldIndex.keys():
              data1=newIndex[key]
              data2=oldIndex[key]
              # identical entries using newer timestamps. 
              if (int(data1[1]) == int(data2[1])  and int(data1[2]) == int(data2[2]) and int(data1[0]) <= int(data2[0]) ): continue   
              # no timestamp requirement 
              #if (int(data1[1]) == int(data2[1])  and int(data1[2]) == int(data2[2])): continue
              #print("Process key in new index=",key)
              article1=data1[3]  # list of articles in new sites 
              article2=data2[3]  # list of articles in old site 
              # print(len(article1),len(article2))

              # convert article2 to dictionary
              olddic={}
              for art in article2:
                    olddic[art[2]] = (art[0],art[1]) 

              #print(olddic)
              #check missing for this publisher 
              missinglist=[]
              for art in article1:
                  title=art[2]
                  if title in olddic:
                    infodic=olddic[title]
                    if (int(art[1]) == int(infodic[1]) and int(art[0]) <= int(infodic[0])): continue # check size and time in case of updates 
                  missinglist.append([title,int(art[0])]) # article title and time       
              if (len(missinglist)>0): missing[key]=missinglist
              #print("Missing articles=",missinglist)

         else:  # this is a new publisher that does not exist!

             data1=newIndex[key]
             article1=data1[3]  # list of articles in new sites
             missinglist=[]
             for art in article1:
                 title=art[2]
                 missinglist.append([title,int(art[0])]) # article title and time
             if (len(missinglist)>0): missing[key]=missinglist


  return missing



def urlSafe(url):
    """Make URL safe for downloads"""
    url=url.replace('%',"%25") # URL save for %
    url=url.replace('#',"%23") # URL save for #
    url=url.replace('"',"%22") # URL save for " 
    url=url.replace('(',"%28") # URL save for ( 
    url=url.replace(')',"%29") # URL save for #
    url=url.replace(' ',"%20") # URL save for space 
    return url

###############################################################
################## remove ZWI files ###########################
###############################################################
def deleteZwiFiles(basedirZWI, zwiFiles=[],  publist=[]):
   """ Remove ZWI files from the disk and the index. Specify the list of ZWI files (without extension) and the publisher from which the files should be removed.  """
   globalIndex=getIndex(basedirZWI)
   pubkeys=[]
   pubdata=[]
   # fetch the publisher
   for key in globalIndex:
        keys=key.split("/")
        if (keys[0] in publist):
              pubdata.append(globalIndex[key])
              pubkeys.append(key) # name of the publisher/directory 

   if (len(pubdata)==0):
       print("i\nError: The publishers:",publist,"are not found")
       return 

   err=""
   ntot=0
   nremove=0
   # now scan all entries in the local index and remove the ZWI file
   # we also remove file phisically if there is anything to remove
   for xdir in pubkeys:
          pub_index=basedirZWI+"/"+xdir+".csv.gz"
          # print("Read:",pub_index) 
          if (exists(pub_index) == False):
            err="\nError: File not found"+pub_index+" Abort removal for ZWI files"; print( bcolors.WARNING + err + bcolors.ENDC ) 
            return
          articles=[]
          with gzip.open(pub_index,'rt') as f:
            for line in f:
              line=line.replace("\n","")
              lines=line.split("|")
              if (len(lines) !=3):
                  err="\nError:Problem for reading the index file:"+pub_index; print( bcolors.WARNING + err + bcolors.ENDC ) 
                  return 
              zwiname=lines[2].strip()
              if (zwiname in zwiFiles):
                  zwipath=basedirZWI+"/"+xdir+"/"+zwiname+".zwi"
                  if (exists(zwipath)):
                       os.rename(zwipath,basedirZWI+"/trash/"+zwiname+".zwi") 
                       print ("  -> ZWI file:",xdir+"/"+zwiname+".zwi removed to /trash/")
                       nremove=nremove+1 
                       continue 
                  else:
                       err="\nWarning: File:"+xdir+"/"+zwiname+".zwi was found in the index but not on the disk."
                       nremove=nremove+1 
                       continue 

              articles.append(lines)
              ntot=ntot+1

          if (len(err)>1):
                  print( bcolors.WARNING + err + bcolors.ENDC ) 
          if (nremove==0):
                  err="\nWarning: Nothing to remove. Index file is unchanged"
                  print( bcolors.WARNING + err + bcolors.ENDC ) 
                  return

          #print(key,"gets",len(articles)," articles")
          xmap=globalIndex[xdir]
          globalIndex[xdir] = [xmap[0],xmap[1],xmap[2],articles]

   print("Nr of ZWI files removed:",nremove)
   print("The size of main index is {} articles".format(ntot),"in",len(globalIndex),"publishers")

   # now write the global index file
   writeIndexFiles(basedirZWI,globalIndex,publist)
   return


if __name__ == "__main__":

  #### input parameters ####
  kwargs = {}
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", '--input', help="Input ZWI directory that ends as ZWI/en to create index files by rescanning directories")
  parser.add_argument("-p", '--publisher', help="Update the index for the list of publishers by rescanning their directories. Global index will be updated too. The names of publishers should be separated by the commas. Example: -p wikipedia,citizendium")
  parser.add_argument("-e", '--exclude', help="Exclude publishers matching a pattern. Use ',' for separation. Example: 'wikipedia,citizendium,handwiki'")
  parser.add_argument("-d", '--delete', help="Delete ZWI files and update the index. Global index will be updated too. The names of zwi file  should be separated by the commas. Do not add extensions. You must to add one publisher from where the ZWI files should be remove. This operation  does not involve re-scanning directories")

  args = parser.parse_args()

  excluded=[]
  if args.exclude!=None:
     excluded=args.exclude.split(",")
     print("Excluded publishers:",excluded)

  publishers=[]
  if args.publisher!=None:
     publishers=args.publisher.split(",")
     print("Included publishers:",publishers)

  basedirZWI=""
  if(args.input != None):
         basedirZWI=args.input
  if(len(publishers)>0 and args.delete == None):
                print("Update the existing index files for the publishers:",publishers) 
  if(args.delete != None):
     if (len(args.delete)>0):
        if(len(publishers)>0): 
            # if you made mistake and added .zwi extension, we correct for this
            filesRemove=[]
            splitted=args.delete.split(",")
            for f in splitted:
                if f.endswith(".zwi"):
                    filesRemove.append( f.replace(".zwi","") )
                else: filesRemove.append( f.strip())
            print("Remove the list of ZWI files:",filesRemove,"from publishers:",publishers) 
            deleteZwiFiles(basedirZWI, filesRemove, publishers) 
            sys.exit() 
        else:       
            err="\nError: These ZWI will be removed:"+args.delete.split(",")+" but the publisher is uknown. Specify the publisher!" 
            print( bcolors.WARNING + err + bcolors.ENDC )
            sys.exit()

  if (len(basedirZWI)<2):
    print("Did not specify the input ZWI directory? This should be the full path that ends with ZWI/en. Use --help")
    sys.exit()

  # excluded (optional) 
  excluded=excluded+["en/sep/","en/greatplains/","en/jewishenc/","en/whe/"]

  # now make new index files for all publishers 
  createIndexFiles(basedirZWI, excluded, publishers)
  print("All done!")
