# Table of Content

[[_TOC_]]

Here is the description of how to setup a network for encyclopedic articles of Encyclosphere. Articles created by the [EncycloReader](https://encycloreader.org) or by external tools can be sent to this network using the URL for file exchange.
In order to make a copy of all articles (and the index files), follow these instructions for Linux:

## How to Install

Run these commands as a user:

```
git clone https://gitlab.com/ks_found/ZWINetwork.git
source ZWINetwork/zwi_network.sh
```

## Creating a mirror  


Now you are ready to join to the network and clone a directory with 
articles in the [ZWI format](https://handwiki.org/wiki/ZWI_file_format). First, let's test the program by running the command: 

`zwi_demo`

This command populates the "ZWI/en" directory with the articles in the ZWI format from 5 demo repositories.
The demo repositories are:

 - http://207.231.104.238/site1/ZWI/
 - http://207.231.104.238/site2/ZWI/
 - http://207.231.104.238/site3/ZWI/
 - http://207.231.104.238/site4/ZWI/
 - http://207.231.104.238/site5/ZWI/

These repositories contain several publishes with different number of ZWI files.  The program will accumulate
all ZWI files from these repositories and create a new (bigger) repository with all articles.  

After download, the program will start monitoring the remote directories in real-time. If some changes are detected in remote ZWI files,
new files will be downloaded to the local repository "ZWI/en". 

The goal of this command is to check that you can mirror ZWI files and create a new mirror.
One can stop the download using CTRL+C (and start the program again at any time).

If the demo example works, one can run the program to download ZWI files from the encycloreader.org which has one of the largest
collection of the articles from Encyclosphere. Run this command: 

`zwi_get`

This command takes some time since it will download more than 100 GBs of files to the directory "ZWI".
After the initial download, the ZWI/en directory will be monitored for any change.
You can use this example to run this command in background:

`nohup zwi_get > zwi.log 2>&1 &` 

You can also download ZWI files without the monitoring step. In this case, run this command: 

`zwi_get -q -o ZWI -i https://encycloreader.org/db/ZWI/`  

If you run the same command again, it will update the ZWI/en directory (or will do nothing).

To see a summary of the network (with mirrors, publisher and the number of files), use this command:

`zwi_get -s`

This command prints the list of mirrors, the numbers of articles and the last update. One can also print the summary of a given mirror:

`zwi_get -s -i https://enhub.org/ZWI/`

To print the summary of already downloaded (local) files, use the command:

`zwi_get -s -l`

By default, this summary assumes that ZWI files are located in the local directory "ZWI/en". To change the local directory, use the option "-o dir".

Check available commands as:

`zwi_get -h`

For the input, [URL] is the existing URL with ZWI files and [DIR] is the local directory for downloaded ZWI files.
Typically, this URL should ends with the "ZWI" (all 3 letters are capital). 

You can also download only a specific publisher using the option "-e" (or --exclude).  For example, the command:

`zwi_get -e wikipedia,isbe`

downloads all publishers while excluding Wikipedia and ISBE publishers (note you should use the lower case in these commands). 

One can also download only specific publishers using the option "-p".  As before, use the comma to separate the publishers:

`zwi_get -p wikipedia,handwiki`

will download only Wikipedia and Handwiki articles.

## Add your site to this network

The above approach explains how to import encyclopedic content, i.e  how to make a library of ZWI files from various publishes
on a local computer. 

You can also share your data with others and become a mirror of data that helps downloading data for others.

Start a web server (say, Apache). Then run the script "zwi_get" in the directory  that is accessible  to this web server. 
In this case the web address `http://domain (or IP address)/ZWI/` will be visible on the Internet and available
for all users who want to  download ZWI files. You can use the usual port 80, but also the port 443 (https:) is supported.  
Then submit this internet address site to the Encyclosphere network.  
Use the [site upload web page](https://encycloreader.org/sites/) for this. After upload of your URL, the list with your mirror will become available for all users who are running the "zwi_get" command.
 
## Add articles to the network using the push method 

In addition to the mechanism of exporting downloaded 
ZWI files using the port 80 of your domain, you can also submit your ZWI files to encycloreader's domain.
There are two methods to publish private ZWI files on the network. 


The first method "push". It will uploads a ZWI file
to the known location in 
[encycloreader.org](https://encycloreader.org). 
If the article will be pushed to encycloreader, it will be properly indexed for searches and will also be exported.
This method is used by the Mediawiki and DokuWiki plugins that "push" the articles directly to the network.

If you have an article in the ZWI format, you can publish it the network under the encycloreader.org domain. 
A file "article.zwi" can be added using the command-line as:

`zwi_push article.zwi`

Use "_" for spaces in the file name. This command makes a few basic checks of the ZWI files, pushes it to the network and updated the index files.  

If no password is given, the above command submits the article to the Encyclosphere "sandbox" (assuming that the field Publisher is set to "sandbox").
If the ZWI file includes the name of the publisher other than "sandbox", the submission will be rejected since no password was specified.
Submission with the password is shown below: 

`zwi_push article.zwi [password]`

For example, if the ZWI file was created with the publisher field "citizendium", you will need to specify the password for this publisher.
Publishers of encyclopedias can obtain such passwords from the KSF.  

One can also upload a ZWI file using [ZWI upload web interface](https://encycloreader.org/upload/).

## Exporting ZWI files using the pull method 

In addition to the "push" method, there is also a method that is more appropriate for large number of ZWI files.
This method is called "pull". This means the ZWI files will be located on a privite web page, but other people
can pull your files from your web server.

If you have a collection of ZWI files, you can export them to outside  the network using a program called "zwi_ini".
How to create ZWI files is described in https://gitlab.com/ks_found/ZWIBuilder (for the Mediawiki-style encyclopedias).

Run the command:

`zwi_ini`

You will need to answer a few questions about where to find your ZWI files, and where ZWI files will be located for exporting
them to outside.

Then start a web server (say, Apache). 
In this case the web address `http://domain (or IP address)/ZWI/` will be visible on the Internet and available
for all users who want to  download  your ZWI files. You can use the usual port 80, but also the port 443 (https:) is supported.
Then submit this internet address site to the Encyclosphere network.
Use the [site upload web page](https://encycloreader.org/sites/) for this. After upload of your URL, the list with your mirror will become available for all users who are running the "zwi_get" command.

## Creating a mirror using Python

One can create a mirror with ZWI files using Python as:

`python3 $ZWI_DIR/zwi_share.py -i https://encycloreader.org/db/ZWI/en/zwi -o ZWI`

This will download all ZWI files in the directory ZWI. Keep this command running since it updates this repository in real time.


## Organization of ZWI files

ZWI files are located in the directory "ZWI". It has sub-directories that correspond to the names of encyclopedias extracted from the domain names. A lowercase is generally used. 
For articles in English, the directory structure is:


```
ZWI/en/ballotpedia/
ZWI/en/citizendium/ 
ZWI/en/wikipedia/
ZWI/en/edutechwiki/ 
ZWI/en/encyclopediamythica/ 
ZWI/en/trash/
ZWI/en/sandbox/
....
```

After the directory that specifies "publisher", there is another directory that corresponds to the domain names supported by this publisher.
A publisher may have sebveral domains. The general directory structure is given by

```
ZWI/[lang]/[publisher]/[domain]/[path]#[name].zwi 
```

(see the next section for examples). The directory "ZWI" also  includes several index files and the LICENSE file: 

1. index.d - plain text index file
2. index.sqlite - SQLlite database with all files
3. index_bad.d  - plain text of titles  that have potential problems
4. LICENSE - licenses


Read about the ZWI file directory structure in the [Encyclosphere documentation project](https://docs.encyclosphere.org/#/zwi-format).

## ZWI file validation
You can validate ZWI file using [ZWI validation](https://encycloreader.org/checkzwi/) tool. It checks the basic syntax and the signature (if it is used).

## ZWI file name convention 

ZWI file names are created using the URL part without the domain name (which, typically, corresponds to the
name of the directory of a specific encyclopedia). In many cases, this URL part corresponds
to the title of the articles. The symbol ";" and  the  page fragment symbol (#) in the URL should not be present (or "#" will be replaced by "/" in the URL). 

If the URL part (without the domain name) has the symbol "/", it will be replaced by the symbol "#" in the ZWI file names to avoid the clash with the directory separation symbol for the Linux platform. 

The ZWI files are stored using the 2-level directory structures.
The directory structure can be used to determine the original URL of articles.
Here are examples of the URL mapping to the ZWI file names:


| URL of the article | ZWI file name |
|-----|----------|
| `https://citizendium.org/Example`              |  ZWI/en/citizendium/citizendium.org/Example.zwi             |
| `https://citizendium.org/Articles:Example`     |  ZWI/en/citizendium/citizendium.org/Articles:Example.zwi    |
| `https://en.wikipedia.org/wiki/QCAD`           |  ZWI/en/wikipedia/en.wikipedia.org/wiki#QCAD.zwi            |
| `https://encyclopedia.com/article/Draw.html`   |  ZWI/en/encyclopedia/encyclopedia.com/article#Draw.zwi     | 

(the URLs shown in this tables are examples only). Note that ".html" or ".htm" are removed when creating the ZWI file names. 

Read about the ZWI file name convention  in
the [Encyclosphere documentation project](https://docs.encyclosphere.org/#/zwi-format).
The above mentioned directory structure is useful when a publisher maintain several domains.

Alternatively,  the directory structure can be created by setting a "publisher" to  the domain name.
This is useful where there is no specific publisher, or the domain keeps multiple encyclopedias organized 
in dedicated directories.

Here is a typical example when publisher=domain, while multiple encyclopedia (encyclopedia1, encyclopedia2) 
are located in the URL path: 

| URL of the article | ZWI file name |
|-----|----------|
| `https://oldpedia.org/encyclopedia1/Example1.html`     |  ZWI/en/oldpedia.org/encyclopedia1/Example1.zwi             |
| `https://oldpedia.org/encyclopedia2/wiki/Example2.html`     |  ZWI/en/oldpedia.org/encyclopedia2/wiki#Example2.zwi    |

As before, this option allows to have 2-level directory structure, and it also allows a unambiguous mapping  of URLs to the file system.


## Indexing your local collection of ZWI files

All ZWI files in the network are organized in the tree-like structure. This structure is described by the index files.
One can create index files for the collection of ZWI files using this command: 

```bash 
zwi_index -i <path to the local durectory>/ZWI/en
```

This command will create the 2-level index files to be described below. 


## Indexing using Python3 or Pypy3

One can create the global and the local
index files of the repository using Python3 or Pypy3 (faster!):

```bash
pypy3 make_index.py -i /dir/.../ZWI/en
```
The command creates all indexes in the network as described above. The second argument is the path
to the directory with the ZWI files. One can also use "Python3" but it will be a 40% slower.

You can also update index of a list of specific publishers. Use this command to update "handwiki" and "citizendium",
but ignoring all other publishes.

```bash
pypy3 make_index.py -i /data/encyclosphere/ZWI/en/ -p "handwiki,citizendium"
```

Note that this command reads the existing index files, and re-scan the directories for the 2 publishers given in the comma separated list. This will be much faster than recreating the index files of all publishers.

One can also remove ZWI files on the disk, with the full update of all index files. Here is the example:

```bash
pypy3  make_index.py -i /data/encyclosphere/ZWI/en/ -p "handwiki" -d "wiki#Data,wiki#QuarkMeet"
```

This command removes two ZWI files with the name "wiki#Data" and "wiki#QuarkMeet" from the publisher "handwiki". Use the comma to separte the ZWI files. Note that the extension ".zwi" can be dropped. The files are not removed but moved to the "trash" directory.
Note that this command is fast since it does not re-scan directories.


## Index file structure

The main (global) index file in the root directory (typically inside ZWI/en) is called "index.csv.gz". It has the following structure: 

```bash
timestamp|total size in bytes|number of files|publisher/domain1 
timestamp|total size in bytes|number of files|publisher/domain2 
```
All entries are sorted according to the timestamp. The first line is the most recent update  from a given publisher.
Thus if you want to know when the directory has changed last time (and for which publisher), simple look at the first entry of this global index.
The last column points to the directory with the ZWI files, i.e  ZWI/en/publisher/domain1 etc. 

The directory "publisher" includes the 2nd level (or local) index files 
with the name "domain1.csv.gz" etc.  They describe the files located in ZWI/en/publisher/domain1. 
The entries in such files are organized as:

```bash
timestamp|filesize|name of the ZWI file without the extension .zwi
```

As for the global index,  the entries are organized the descending ordering according to the timestamp of files.
The most recent ZWI file goes first. 

Thus, if you want to know which file is the most recent in the network, look at the first entry 
in the global index "index.csv.gz". It will point to the 2nd level index file. Its first entry will
show the most recent ZWI file. 

## Status of the network

The best way to view the status of uploaded ZWI files is to use the [ZWI network monitoring webpage](https://encycloreader.org/db/)


## License

GNU Lesser General Public License v3.0
https://www.gnu.org/licenses/lgpl-3.0.en.html

## Project status

 
 - Version 1.0, Jan 2, 2023
 - Version 0.9, Sep 22, 2022

