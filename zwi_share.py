#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#
# Sare  ZWI files on a local file system.
# It also updates the mirror in real time.
# Check all options as:  python3 zwi_share.py -h 
# Note all times are in UTC. 
# Testing: 
# http://69.197.158.246/site1/ZWI/
# http://69.197.158.246/site2/ZWI/
# http://69.197.158.246/site3/ZWI/
# http://69.197.158.246/site4/ZWI/
# http://69.197.158.246/site5/ZWI/

#
# Changelog:
# ---------
# version 1.2, Jan 13, 2023: Handling retries in requests 
# version 1.1, Dec 24, 2022: Update functionality
# version 1.0, Nov 30, 2022: Fist draft to show the concept. 
# = S.Chekanov (KSF) = 
#

import os,time,sys,io,gzip,os.path
from datetime import datetime,timezone
import requests,urllib,urllib3
from requests.adapters import HTTPAdapter
from urllib3 import Retry
import shutil,argparse 
import signal,time,glob,re
# import all modules from this directory 
script_dir=os.path.dirname(os.path.realpath(__file__))
sys.path.append(script_dir)
from make_index import *

#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument('-q', '--quiet', action='store_true', help="don't show verbose")
parser.add_argument('-o', '--output', help="Save output to the specified directory")
parser.add_argument("-i", '--input', help="Input URL of the existing ZWI database")
parser.add_argument("-s", '--summary', action='store_true', help="Prints the summary of the remote network files and exit.")
parser.add_argument("-p", '--publisher', help="Select publishers matching a pattern. Use ',' for separation. Example: 'wikipedia,citizendioum,handwiki'")
parser.add_argument("-e", '--exclude', help="Exclude publishers matching a pattern. Use ',' for separation. Example: 'wikipedia,citizendioum,handwiki'")
parser.add_argument("-l", '--local', action='store_true', help="Prints the summary of local directory when using the option \"-s\". Use \"-o\" to point to the local directory. ")
parser.add_argument("-m", '--monitoring', action='store_true', help="Monitoring all available sites and accumulate results")

args = parser.parse_args()
isVerbose=True
if (args.quiet): isVerbose=False;

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)

if args.local == False:
   print("Input URL  =",args.input)

excluded=[]
if args.exclude!=None:
  excluded=args.exclude.split(",")
  print("Excluded publishers:",excluded)

publishers=[]
if args.publisher!=None:
  publishers=args.publisher.split(",")
  print("Included publishers:",publishers)

print("Is quiet =",args.quiet)

# default time monitoring (in min)
TimeDelayMin=0.5; # 1/2 of a minute (30 seconds)  

# active URL sites
activeSites=[]

# default URL
# root_repo="https://encycloreader.org/db/ZWI/"

# debug
root_repo="http://69.197.158.246/site1/ZWI/" 

if args.input!= None:
        if len(args.input)>0:
               root_repo=args.input


# output directory
output="ZWI"
if args.output!= None:
        if len(args.output)>0:
               output=args.output 

################## catch Ctrl+C ###############
def signal_handler(sig, frame):
    global excluded, publishers, output, root_repo 
    """ action when pressing Ctrl+C """
    print('\nPressed [Ctrl]+[C] to interrupt the program')
    print('Save the current status and exit')
    createIndexFiles(output, excluded, publishers, verbose=False)
    copyAuxilary(output, root_repo, verbose=False)
    sys.exit(0)

# handle exit
signal.signal(signal.SIGINT, signal_handler)
# signal.pause()


root_repo=root_repo.strip()
output=output.strip()
if root_repo.endswith("/") == False:
    root_repo=root_repo+"/"

if args.local == False:
  print("Main site URL    =",root_repo)
  print("Output directory =",output)
  print("Start at",datetime.now(),"(local time)")

# main download area
main_repo=root_repo+"en/"
root_repo_default=root_repo
main_repo_default=main_repo

# just check that everything is fine:
r = requests.head(root_repo+"en/index.csv.gz")
if r.status_code == requests.codes.ok:
    pass
else:
   print("")
   print("ERROR")
   print("Cannot find index.csv.gz in this URL")
   print("It is possible you do not give correct location to ZWI database. Exit!")
   sys.exit(1)


# Colored printing functions for strings that use universal ANSI escape sequences.
# fail: bold red, pass: bold green, warn: bold yellow,
# info: bold blue, bold: bold white
@staticmethod
def print_fail(message, end = '\n'):
      print('\x1b[1;31m' + message.strip() + '\x1b[0m' + end)
def print_pass(message, end = '\n'):
       print('\x1b[1;32m' + message.strip() + '\x1b[0m' + end)
def print_warn(message, end = '\n'):
     print('\x1b[1;33m' + message.strip() + '\x1b[0m' + end)
def print_info(message, end = '\n'):
       print('\x1b[1;34m' + message.strip() + '\x1b[0m' + end)
def print_bold(message, end = '\n'):
       print('\x1b[1;37m' + message.strip() + '\x1b[0m' + end)

##############################################
# Compare 2 dictionaries d1 (new) with d2 (old)
##############################################
def dict_compare(d1, d2):
  # compare 2 dictionaries. d1 is new dictionary, d2 is old dictionary  
  d1_keys = set(d1.keys())
  d2_keys = set(d2.keys())
  shared_keys = d1_keys.intersection(d2_keys)
  added = d1_keys - d2_keys
  removed = d2_keys - d1_keys
  modified = {o : (d1[o], d2[o]) for o in shared_keys if d1[o] != d2[o]}
  same = set(o for o in shared_keys if d1[o] == d2[o])
  return added, removed, modified, same

############################################
# Download a file from URL to location
# input: URL of ZWI
#        Location on the disk 
# Return: None
###########################################
existingDirs={}


#############################################
def copyAuxilary(output, baseURL, verbose=True):
    """ Copy auxilary files to make the network visible """
    path=output+"/en"
    if (path.endswith("/en")==False  and path.endswith("/en/")==False):
                              path=path+"/en"

    urlpath=baseURL
    if (urlpath.endswith("en")==False  and urlpath.endswith("en/")==False):
                              urlpath=urlpath+"/en/"

    #print(output)
    #print(baseURL)
    ts = int(time.time())
    fileToCopy={}
    fileToCopy[urlpath+"/index.html"] = [path+"/index.html",ts]
    fileToCopy[baseURL+"/index.txt"] =  [output+"/index.txt",ts]
    fileToCopy[baseURL+"/sites.d"] =    [output+"/sites.d",ts]
    fileToCopy[baseURL+"/LICENSE"] =    [output+"/LICENSE",ts]
    getFileKeepTCP(fileToCopy,"",False) # copy to local
    getSites(baseURL)        # get sites.d 
    if (verbose): print("Sites available:",activeSites)

    # necessary site protection
    shutil.copyfile(output+"/index.txt", output+"/index.php")
    os.chmod(path+"/index.html", 755)
    os.chmod(output+"/en/index.html", 755)
    os.chmod(output+"/index.php", 755)
    os.system("chmod 755 "+output+"/*")


# Download a file from URL and place in the directory
# If the directory does not exist, make it
def getFile(xurl,filename):
           """  Download file from xurl to filename """
           try:
                r = requests.get(xurl, stream = True, timeout=(10, 300))
           except requests.exceptions.RequestException as e:  # This is the correct syntax
               print(e)
               return False 
           except requests.ReadTimeout as e:
               print('Timed out: {}'.format(xurl))
               return False 
           if r.status_code == 200:
              # Set decode_content value to True, file's size will be zero.
              r.raw.decode_content = True

              # make directory if it is not here
              fullpath=os.path.dirname(filename)

              if fullpath in existingDirs.keys():
                      with open(filename,'wb') as f:
                          shutil.copyfileobj(r.raw, f)
                      return True 

              if not os.path.exists( fullpath ):
                          os.makedirs( fullpath )
                          existingDirs[fullpath]=True            
              else:  existingDirs[fullpath]=True
 
              with open(filename,'wb') as f:
                      shutil.copyfileobj(r.raw, f)
              return True 
           else:
              print("Error downloading",xurl)
              print("Server returns the status:",r.status_code)
              return False

# get sites.d to the main site index 
def getSites(xurl,verbose=True):
           global activeSites
           ''' Download sites.d and add it to the main site index.
               Define URL for this site. 
               It returns true. 
           '''
           if (xurl.endswith("sites.d") == False):
                       xurl=xurl+"/"+"sites.d"
           try:
                r = requests.get(xurl, stream = True, timeout=(10, 300))
           except requests.exceptions.RequestException as e:  # This is the correct syntax
               print(e)
               return False 
           except requests.ReadTimeout as e:
               print('Timed out: {}'.format(xurl))
               return False 
           if r.status_code == 200:
              data = r.text
              data = data.split('\n') # then split it into lines
              for line in data:
                   if (line.endswith("ZWI")):
                            line=line+"/"
                   if (line.endswith("ZWI/")):
                            activeSites.append(line)
           activeSites = list(set(activeSites))
           return True 

# Download a file from URL and place in the directory
# Do not check directories. 
# Note: this is a slow method that restarts the TCP connection 
def getFileNoCheck(xurl,filename):
           # download file to location
           try:
                r = requests.get(xurl, stream = True, timeout=(10, 300))
           except requests.exceptions.RequestException as e:  # This is the correct syntax
               print(e)
               return
           except requests.ReadTimeout as e:
               print('Timed out: {}'.format(xurl))
               return
           if r.status_code == 200:
              # Set decode_content value to True, file's size will be zero.
              r.raw.decode_content = True

              with open(filename,'wb') as f:
                      shutil.copyfileobj(r.raw, f)
              return True

def print_progress_bar(index, total, label):
    """ draw progress bar """
    n_bar = 50  # Progress bar width
    progress = index / total
    sys.stdout.write('\r')
    sys.stdout.write(f"[{'=' * int(n_bar * progress):{n_bar}s}] {int(100 * progress)}%  {label}")
    sys.stdout.flush()


###############################################################################
# This method downloads a file from xurl to filename keeping the TCP connection
# alive. Note the xurl and filename are lists with URL and locations 
# Input: dictionary URL:output file
##############################################################################
def getFileKeepTCP(urlsAndfilenames, name="",verb=False):
       global excluded, publishers, output, root_repo

       """ download files using the same session. If name is given draw status bar.
           Input: dictionary dic[url]=[path,timestamp]
                  name: name for this download publisher/publisher.org  
       """
       #print(urlsAndfilenames)
       # four retries (in addition to the original request)
       # backoff factor of 1 for the retry delay 
       # retry with all HTTP methods
       # retry with HTTP status codes 429, 500, 502, 503 and 504 only
       # Also see https://majornetwork.net/2022/04/handling-retries-in-python-requests/
       sessionTCP  =  requests.Session()
       #adapter = HTTPAdapter(max_retries=Retry(total=4, backoff_factor=1, allowed_methods=None, status_forcelist=[429, 500, 502, 503, 504]))
       adapter = HTTPAdapter(max_retries=Retry(total=4, backoff_factor=1, status_forcelist=[429, 500, 502, 503, 504]))
       sessionTCP.mount("http://", adapter)
       sessionTCP.mount("https://", adapter)
       start_time = time.monotonic() 

       total = len(urlsAndfilenames.items())
       if (len(name)>1): 
              verb=True
              fx=name.split("/")
              if (len(fx)>1): name=fx[0]
       n=0
       try:
         for key, value in urlsAndfilenames.items():
              # print(key, '->', value)
              redata = sessionTCP.get( key  )
              fipath=value[0]
              fitime=int(value[1])
              xfile = open(fipath, "wb")
              xfile.write(redata.content)
              xfile.close()
              os.utime(fipath, (fitime,fitime))
              if (verb):
                 n=n+1
                 print_progress_bar(n, total, name)
       except Exception as e:
               print(type(e))
               print(e)
               print("There was a crash. We will attempt to save the status. Please restart the network script")
               createIndexFiles(output, excluded, publishers, verbose=False)
               copyAuxilary(output, root_repo, verbose=False)
               sys.exit(0)
       if (verb): print(" ")

       stop_time = time.monotonic()
       sectaken=round(stop_time-start_time, 2)
       if (sectaken>10): print(round(stop_time-start_time, 2), "sec required")

       sessionTCP.close()

###############################################################################
# Create output directories from the file lists
# Input:  Global path for output 
#         List of remote ZWI files from index.d.gz
##############################################################################
def makeOutputDirectories(path,filelist):
    """ Create output directories from the file list"""
    allDirs={} # list of directories to be created
    for line in filelist:
        line=line.replace("\n","")
        allDirs[os.path.dirname( line )]=1

    # make directories 
    for key in allDirs.keys():
       xdir=path+"/"+key
       if (os.path.exists(xdir) == False):
          cmd="mkdir -p "+path+"/"+key
          os.system(cmd)


def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   #size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   size_name = ("MB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])


## print network summary
if (args.summary):

    # first check if files already exist
    path=output
    if (path.endswith("/en") == False and path.endswith("/en/") == False):
          path=output+"/en"
    urlindex=root_repo
    if (urlindex.endswith("/en") == False and urlindex.endswith("/en/") == False):
          urlindex=urlindex+"/en"

    # look at local directory
    if (args.local):
           allDirs={}
           allUrl=[]  # URLs for wget
           print("[MSG] == ZWI directory summary == ")
           print("[MSG] : Local directory :",output)
           files = glob.glob(path+"/**/*.zwi", recursive=True) 
           for line in files:
              allDirs[os.path.dirname( line )]= 1
              allUrl.append(line)
           print("[MSG] : Publishers:")
           nke=0
           ntotf=0
           print ("  {:<5} {:<10} {:<15}".format('Nr','Files','Publisher'))
           print ("  {:<5} {:<10} {:<15}".format('--','-----','---------'))
           for key in allDirs.keys():
               skey=key.replace(output+"/en/","")
               nke=nke+1
               vtot=0
               for xli in allUrl:
                  matches = re.findall(key, xli)
                  vtot=vtot+len(matches)
               print ("  {:<5} {:<10} {:<15}".format(nke,vtot,skey))
           print("[MSG] : Total Nr of files =",len(allUrl))
           sys.exit()


    # now look at remote directories
    print("[MSG] == ZWI Network summary == ")
    print("[MSG] : Main repository:",root_repo)
    globalIndex=getIndexRemote(urlindex,verbose=True)

    sys.exit()


###############################################
####### fresh download ######################## 
###############################################
def getIndexFromSiteMakeDirectories(output,url_site,excluded=[],publishers=[],verbose=True):
    """ Download all indexes from the remote sizes and create the needed directories.
        It assumes that the disrectory does not exists.
        @return False if the directory ZWI/en exists 
    """  
    # first check if files already exist
    path=output
    if (path.endswith("/en") == False and path.endswith("/en/") == False):
          path=output+"/en"

    urlindex=url_site
    if (url_site.endswith("/en") == False and url_site.endswith("/en/") == False):
          urlindex=urlindex+"/en"

    if not os.path.exists(path):
      os.makedirs(path)
      if (verbose): 
           print("[MSG] create:", path)
           print("[MSG] Batch download to:", path)
      #print(urlindex+"/index.csv.gz")
      stat=getFile(urlindex+"/index.csv.gz",   path+"/index.csv.gz")
      if (stat==False):
           print("Cannot download index file!")
           sys.exit() 
    else:

       if not os.path.exists(path+"/index.csv.gz"):
           print("There is an error in downloading:", path+"/index.csv.gz")
           sys.exit()


       err=False
       with gzip.open(path+"/index.csv.gz", 'r') as fh:
         try:
           fh.read(1)
         except OSError:
           err=True
         if (err==False):
                  if (verbose): print("[MSG] path:", path+" already exists")
                  return err; # returns false if previous installation is detected 

    mainIndex={}
    # read global index
    with gzip.open(path+"/index.csv.gz",'rt') as f:
        for line in f:
           line=line.replace("\n","")
           lines=line.split("|")
           if (len(lines) !=4):
               print("Problems in reading index file:",outfile)
               sys.exit()

           if (len(excluded)>0):
               remove=False
               for ex in excluded:
                   if (lines[3].find(ex)>-1): remove=True
               if (remove): continue

           if (len(publishers)>0):
               keep=False
               for ex in publishers:
                 if (lines[3].find(ex)>-1): keep=True 
               if (keep==False): continue

           mainIndex[lines[3]]=[ lines[0],lines[1], lines[2] ]
           # print("TEST=",lines[3])

    globalIndex={}
    urlsAndfilenames={}
    # read data for specific publishers
    for key in mainIndex:
          pub_index=key+".csv.gz"
          data=mainIndex[key]
          modtime=int(data[0]) # modification time from source 
          urlsAndfilenames[urlindex+"/"+pub_index] = [path+"/"+pub_index, modtime] 
          # print("Read=",pub_index,modtime)
          ldir=path+"/"+key
          if not os.path.exists(ldir):
                  os.makedirs(ldir)
    getFileKeepTCP(urlsAndfilenames,"",False) # download local index files to correct places 

    # download ZWI files 
    for key in mainIndex:
          pub_index=path+"/"+key+".csv.gz"
          if not os.path.exists( pub_index ):
             print("There is an error in finding:", pub_index)
             sys.exit()



          urlsAndfilenames={}
          # download articles for a given publisher
          with gzip.open(pub_index,'rt') as f:
           for line in f:
              line=line.replace("\n","")
              lines=line.split("|")
              if (len(lines) !=3):
                  print("Problems reading index file:",pub_index)
                  return 
              modtime=int(lines[0])
              zwifile=path+"/"+key+"/"+lines[2]+".zwi" 
              url=urlindex+"/"+key+"/"+lines[2]+".zwi"
              url=urlSafe(url) # make URL safe for downloads 
              zwifile=zwifile.replace('"', '\"')
              urlsAndfilenames[url]=[zwifile,modtime] 
          #print()
          #print(pub_index)
          #print(urlsAndfilenames)
          getFileKeepTCP(urlsAndfilenames,key,True)
    return True


######################################################
############## update ZWI files ######################
######################################################
def pushUpdate(output,repourl,excluded=[],publishers=[],verbose=True):
       """ update existing ZWI/en directory using the index files 
       output: str
               Output directory (ZWI/en)
       repourl: str
                URL from where update should be sourced 
       excluded: list
                 List with excluded publishers 
       publishers: list
                 List of allowed publishers 
       returns: dict{} 
                returns updated global index 
       """

       globalIndex={} 
       outputdir=output
       if (output.endswith("en")==False  and output.endswith("/en/")==False):
                              outputdir=output+"/en/"
       if (repourl.endswith("en")==False  and repourl.endswith("/en/")==False):
                              repourl=repourl+"/en/"


       if (verbose==True): 
           print_warn("## ZWI repository detected on local file system","")
           print_warn("## Re-scan: "+outputdir,"")
       # currentIndex=createIndexFiles(outputdir, excluded, verbose) # get current index and write it to the directory 
       # print("Local summary=", shortSummaryOfIndex(currentIndex))
       currentIndex=getIndex(outputdir,excluded, publishers,verbose)     # read current local index file from file system 
       remoteIndex =getIndexRemote(repourl,excluded, publishers,verbose) # get remote index file 
       # print("Remote summary=", shortSummaryOfIndex(remoteIndex))
       # calculate delta of the current and online index files

       deltaList=getDeltaDifference(remoteIndex, currentIndex, excluded, publishers, verbose)
       #print("Nr of Missing=",len(deltaList))

       if (len(deltaList)>0):
                    # read data for specific publishers
                    for key in deltaList:
                           if (len(key.strip())<1): continue 
                           if (verbose): print("Update",len(deltaList[key]),"files for",key)
                           ldir=outputdir+"/"+key
                           if not os.path.exists(ldir):
                                         os.makedirs(ldir)
                                         if (verbose): print(ldir," does not exist. Make it!")
                           urlsAndfilenames={}
                           listofzwi=deltaList[key]
                           if (len(listofzwi)==0): continue
                           for f in listofzwi:
                                fipath=f[0]
                                fitime=int(f[1])
                                zwi_site_url=repourl+key+"/"+fipath+".zwi"
                                zwi_local_path=outputdir+"/"+key+"/"+fipath+".zwi"
                                zwi_site_url=urlSafe(zwi_site_url) # make URL safe for downloads 
                                #zwi_local_path=zwi_local_path.replace('"', '\\"')
                                urlsAndfilenames[zwi_site_url] = [zwi_local_path,fitime]
                                #print(zwi_site_url, zwi_local_path)
                           #print(urlsAndfilenames,key)
                           getFileKeepTCP(urlsAndfilenames,key,False)
                    globalIndex=createIndexFiles(outputdir, excluded, publishers, verbose) # create new index  
       return globalIndex 

#######################################################
# Main part starts
#######################################################
# get all index files and create the needed directories
verbose=isVerbose
print_info("To stop press [Ctrl] + [C]","")
freshDownload=getIndexFromSiteMakeDirectories(output,root_repo,excluded,publishers,verbose)
if (freshDownload==False and args.monitoring==False):
        print("Previous directory detected. Update!") 
        pushUpdate(output,root_repo,excluded,publishers,verbose) # GET ZWI files for update 
        getSites(root_repo)                           # get sites.d
        


## copy auxilary files to make the network visible
copyAuxilary(output, root_repo, verbose=True)


if (args.monitoring==False): 
    print("Finish mirroring at ",datetime.now())
    sys.exit() 

## monitoring loop
nsites="1("+str(len(activeSites))+")"
################################# 
# main monitoring loop with sleep
#################################
n=0
print("")
print_info("Monitoring:  press [Ctrl] + [C] to stop","")

while True:
    xnow = datetime.now()
    time.sleep(60*TimeDelayMin)
    current_site=activeSites[n];
    nsites=str(n+1)+"("+str(len(activeSites))+")"
    if (verbose): print("Current site:",current_site);
    getSites(current_site,verbose)                   # get sites.d
    #if (freshDownload==False): freshDownload=getIndexFromSiteMakeDirectories(output,root_repo,verbose)
    globalIndexTMP=pushUpdate(output,current_site,excluded,publishers,verbose) # GET ZWI files for update
    if (len(globalIndexTMP)>0): 
      xsum=shortSummaryOfIndex(globalIndexTMP)
      sss=str(int(xsum[2]))+" articles, "+str(int(xsum[0]))+" publishers" 
      print("->",xnow.strftime("%Y-%m-%d %H:%M:%S"),nsites,"sites",sss)
    else:
      print("->",xnow.strftime("%Y-%m-%d %H:%M:%S"),nsites,"sites. No updates")
    n=n+1
    if (len(activeSites)==n): n=0;


print("Finish monitoring at ",datetime.now())

