#!/bin/bash
# Initialize a repository to share ZWI
# Version 1.2, Jan 29, 2023
#	Move to local/share/zwi
#
# S.Chekanov  + KSF

if [ ! -x /usr/bin/curl ] ; then
    command -v curl >/dev/null 2>&1 || { echo >&2 "Please install 'curl' or set it in your path. Aborting."; exit 1; }
fi

if [ ! -x /usr/bin/rsync ] ; then
    command -v rsync >/dev/null 2>&1 || { echo >&2 "Please install 'rsync' or set it in your path. Aborting."; exit 1; }
fi


if [[ "$(python3 -V)" =~ "Python 3" ]]; then
   echo ""
else
   echo "Python 3 is NOT installed. Exit"
   exit 1
fi 

bold=$(tput bold)
normal=$(tput sgr0)

CUR_DIR=`pwd`

export ZWI_DIR="$HOME/.local/share/zwi/"

if [ ! -x "$ZWI_DIR" ] ; then
    mkdir -p $ZWI_DIR
fi

echo "This script helps setting up ithe disrectory structure for ZWI export" 
echo ""
echo -n "${bold}Define the directory with ZWI files:${normal}"
read userInput
inZWI=""
if [[ -n "$userInput" ]]
then
    inZWI=$userInput
fi
echo "Input ZWI files are in $inZWI" 
echo ""

zwiFiles=`ls -1q $inZWI/*.zwi | wc -l`
echo "Nr of ZWI files found: $zwiFiles"

if [ "$zwiFiles" -lt "1" ]; then 
    echo "No ZWI files with the extension .zwi found in $inZWI"
    echo "Exit!"
    exit 0;
fi

echo ""
echo "The network files should be located in a directory visible by the port 80"
echo "The most common directory is /var/www/html (you should have the access to write)"
echo ""
echo -n "${bold}Define the full path of a directory where ZWI files should be copied${normal}:"
read userOutput
outZWI=""
if [[ -n "$userOutput" ]]
then
    outZWI=${userOutput}
fi
echo "Output directory is: $outZWI" 

indexZWI="${outZWI}/ZWI/"
outputZWI="${outZWI}/ZWI/en/"
echo "The main directory for the network: $outputZWI"


# check the permission
if [ -w ${outZWI} ]; then
 echo "ok, ${outZWI} is writable"
else
 echo "You do not have write permission for ${outZWI}"
 echo "Exit!"
 exit 1
fi


if [ ! -x "$outputZWI" ] ; then
    mkdir -p $outputZWI
fi

echo ""
echo "For ZWI files for outside export, you need a name of the publisher."
echo ""
echo -n "${bold}Define the publisher's name:${normal}"
read userPublisher
outPub=""
if [[ -n "$userPublisher" ]]
then
    outPub=${userPublisher}
fi

outPub="$outPub" | awk '{print tolower($0)}'

echo "Publisher name: $outPub" 

echo ""
echo "Each publisher should have the domain name where the articles are located in some HTML form"
echo ""
echo -n "${bold}Define the publisher's domain name where articles can be read:${normal}"
read userDomain 
outDomain=""
if [[ -n "$userDomain" ]]
then
    outDomain=${userDomain} 
fi
echo "Domain name of this publisher: $outDomain"

CopyDir=${outputZWI}/${outPub}/${outDomain} 
if [ ! -x "$CopyDir" ] ; then
    mkdir -p $CopyDir
fi

echo "Copy from $inZWI/ to $CopyDir/"
# rsync -a --prune-empty-dirs --include "*/"  --include="*.zwi" --exclude="*" ${inZWI}/ ${CopyDir}/
cp -f ${inZWI}/*.zwi ${CopyDir}/

# make index
python3 $ZWI_DIR/make_index.py -i $outputZWI

# fill with Aux files
curl -s https://encycloreader.org/db/ZWI/index.txt -o $indexZWI/index.php
curl -s https://encycloreader.org/db/ZWI/sites.d -o   $indexZWI/ZWI/sites.d
curl -s https://encycloreader.org/db/ZWI/LICENSE -o   $indexZWI/ZWI/LICENSE
chmod -R 755 $indexZWI

echo ""
echo "Creation of the public export is finished"
echo "========================================="
echo "The ZWI files are located in $CopyDir"
echo "The global index file is in $outputZWI"
echo "(1) Make sure the file $outZWI/ZWI/index.php is visible by the web server"
echo "    The Welcome page: http://your domain/../ZWI/"
echo "(2) Submit http://your domain/../ZWI/ site to Encyclosphere"
echo "    Read the description: https://gitlab.com/ks_found/ZWINetwork"
echo "All done!"
